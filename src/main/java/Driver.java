import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Driver extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        ToolRunner.run(new Driver(), args);
    }

    private Job setup() throws IOException {
        Configuration con = new Configuration();
        Job job = Job.getInstance(con, "Driver");
        job.setJarByClass(Driver.class);
        return job;
    }

    @Override
    public int run(String[] strings) throws Exception {
        Job job = setup();
        FileOutputFormat.setOutputPath(job, new Path("/ou0"));
        byte[] EMPLOYEE_TABLE = Bytes.toBytes("Employee");
        byte[] BUILDING_TABLE = Bytes.toBytes("Building");

        List<Scan> scanList = new ArrayList<>();
        Scan employeeScan = new Scan();
        employeeScan.setAttribute(Scan.SCAN_ATTRIBUTES_TABLE_NAME, EMPLOYEE_TABLE);
        scanList.add(employeeScan);

        Scan buildingScan = new Scan();
        buildingScan.setAttribute(Scan.SCAN_ATTRIBUTES_TABLE_NAME, BUILDING_TABLE);
        buildingScan.setCacheBlocks(false);
        scanList.add(buildingScan);

        TableMapReduceUtil.initTableMapperJob(scanList, HbaseMapper.class, Text.class, ImmutableBytesWritable.class, job);
        TableMapReduceUtil.initTableReducerJob("Employee", HbaseReducer.class, job);
        return (job.waitForCompletion(true) ? 0 : 1);
    }
}
