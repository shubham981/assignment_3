import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HbaseReducer extends TableReducer<Text, ImmutableBytesWritable, Text> {
    @Override
    protected void reduce(Text key, Iterable<ImmutableBytesWritable> values, Context context) throws IOException, InterruptedException {
        String EMPLOYEE_TABLE = "Employee";
        byte[] COLUMN_FAMILY = Bytes.toBytes("EmployeeDetails");
        byte[] COLUMN_IDENTIFIER = Bytes.toBytes("EmployeeProto");
        List<Employee.employee> employeeList = new ArrayList<>();
        List<Building.building> buildingList = new ArrayList<>();
        for (ImmutableBytesWritable data : values) {
            ComponentTagger.commonIdentifier commonIdentifier = ComponentTagger.commonIdentifier.parseFrom(data.get());
            if (EMPLOYEE_TABLE.equals(commonIdentifier.getTableString())) {
                employeeList.add(Employee.employee.parseFrom(commonIdentifier.getRowData()));
            } else {
                buildingList.add(Building.building.parseFrom(commonIdentifier.getRowData()));
            }
        }

        if (buildingList.size() == 1) {
            int cafetariaCode = buildingList.get(0).getCafeteriaCode();
            for (Employee.employee e : employeeList) {
                Employee.employee.Builder newEmployeeBuilder = Employee.employee.newBuilder();
                newEmployeeBuilder.setName(e.getName());
                newEmployeeBuilder.setEmployeeId(e.getEmployeeId());
                newEmployeeBuilder.setBuildingCode(e.getBuildingCode());
                newEmployeeBuilder.setFloor(e.getFloor());
                newEmployeeBuilder.setSalary(e.getSalary());
                newEmployeeBuilder.setDepartment(e.getDepartment());
                newEmployeeBuilder.setCafeteriaCode(cafetariaCode);
                Configuration conf = HBaseConfiguration.create();
                Connection connection = ConnectionFactory.createConnection(conf);
                Table hBaseTableObject = connection.getTable(TableName.valueOf(EMPLOYEE_TABLE));
                Put put = new Put(Bytes.toBytes(e.getEmployeeId()));
                put.addColumn(COLUMN_FAMILY, COLUMN_IDENTIFIER, newEmployeeBuilder.build().toByteArray());
                hBaseTableObject.put(put);
            }
        } else {
            if (buildingList.size() == 0)
                throw new RuntimeException("Data Error: No Buildings with same Building code");
            else
                throw new RuntimeException("Data Error: Multiple Buildings with same Building code");
        }

    }
}
