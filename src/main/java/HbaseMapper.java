import com.google.protobuf.ByteString;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableSplit;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;

import java.io.IOException;

public class HbaseMapper extends TableMapper<Text, ImmutableBytesWritable> {
    public enum TABLE_NAME {
        EMPLOYEE,
        BUILDING
    };

    @Override
    protected void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException {
        String EMPLOYEE_TABLE = "Employee";
        String BUILDING_TABLE = "Building";
        byte[] COLUMN_FAMILY = Bytes.toBytes("Employeedetails");
        byte[] COLUMN_IDENTIFIER = Bytes.toBytes("EmployeeProto");

        TableSplit tableSplit = (TableSplit) context.getInputSplit();
        String tableName = tableSplit.getTable().getNameAsString();

        if (EMPLOYEE_TABLE.equals(tableName)) {
            Employee.employee e = Employee.employee.parseFrom(value.getValue(COLUMN_FAMILY, COLUMN_IDENTIFIER));
            ComponentTagger.commonIdentifier.Builder componentBuilder = ComponentTagger.commonIdentifier.newBuilder();
            componentBuilder.setTableName(ComponentTagger.commonIdentifier.identifier.employee);
            componentBuilder.setRowData(ByteString.copyFrom(e.toByteArray()));
            context.write(new Text(e.getBuildingCode()), new ImmutableBytesWritable(componentBuilder.build().toByteArray()));
        } else if (BUILDING_TABLE.equals(tableName)) {
            Building.building b = Building.building.parseFrom(value.getValue(COLUMN_FAMILY, COLUMN_IDENTIFIER));
            ComponentTagger.commonIdentifier.Builder componentBuilder = ComponentTagger.commonIdentifier.newBuilder();
            componentBuilder.setTableName(ComponentTagger.commonIdentifier.identifier.building);
            componentBuilder.setRowData(ByteString.copyFrom(b.toByteArray()));
            context.write(new Text(b.getBuildingCode()), new ImmutableBytesWritable(componentBuilder.build().toByteArray()));
        }


    }
}
